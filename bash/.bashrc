# This is basic comment
# ~/.bashrc
#

# Show inspiration messages
fortune
# If not running interactively, don't do anything
[[ $- != *i* ]] && return

#alias ls="ls --color=always --group-directories-first -p"

## Using exa instead of ls

alias ls="ls --color=always --group-directories-first"

alias grep="grep --color=always"

export GUIX_PROFILE="/home/starfish/.guix-profile"
source "$GUIX_PROFILE/etc/profile"

export GUIX_PROFILE="$HOME/.config/guix/current"
source "$GUIX_PROFILE/etc/profile"

export GUIX_LOCPATH=$HOME/.guix-profile/lib/locale
export SSL_CERT_DIR="$HOME/.guix-profile/etc/ssl/certs"
export SSL_CERT_FILE="$HOME/.guix-profile/etc/ssl/certs/ca-certificates.crt"
export GIT_SSL_CAINFO="$SSL_CERT_FILE"
export CURL_CA_BUNDLE="$HOME/.guix-profile/etc/ssl/certs/ca-certificates.crt"

# configuring the prompts
BLUE="\[$(tput setaf 14)\]"
PINK="\[$(tput setaf 5)\]"
RESET="\[$(tput sgr0)\]"
GREEN="\[$(tput setaf 2)\]"
GREY="\[$(tput setaf 8)\]"
RED="\[$(tput setaf 1)\]"

#PS1="${GREEN}[${RESET}${BLUE}\u${RESET}${PINK}@${RESET}${GREY}\h${RESET} ${RED}\W${RESET}${GREEN}]${RESET}λ "

PS1='[\u \W]λ ' # this is the main(primary) prompt
PS2='> ' # Secondary Prompt

## My personal preferences

#Launch this programs
#neofetch  #shows screen information
#tmux  #launches tmux


# Don't put duplicatel lines or lines starting with space
# in the bash history
HISTCONTROL=ignoreboth

# Append to the history file don't overwrite it
shopt -s histappend
PROMPT_COMMAND="history -a"

# For setting history length size see HISTSIZE
HISTSIZE=1000
HISTFILESIZE=2000

# Check the windows size after each commmand. And if necessary,
# update the values of lines and commands
shopt -s checkwinsize

### ARCHIVE EXTRACTION # usage: ex <file>
ex ()
{
    if [ -f $1 ] ; then
	case $1 in
	    *.tar.bz2)   tar xjf $1   ;;
	    *.tar.gz)    tar xzf $1   ;;
	    *.bz2)       bunzip2 $1   ;;
	    *.rar)       unrar x $1   ;;
	    *.gz)        gunzip $1    ;;
	    *.tar)       tar xf $1    ;;
	    *.tbz2)      tar xjf $1   ;;
	    *.tgz)       tar xzf $1   ;;
	    *.zip)       unzip $1     ;;
	    *.Z)         uncompress $1;;
	    *.7z)        7z x $1      ;;
	    *.deb)       ar x $1      ;;
	    *.tar.xz)    tar xf $1    ;;
	    *.tar.zst)   unzstd $1    ;;
            *)           echo "'$1' cannot be extracted via ex()" ;;
	esac
    else
	echo "'$1' is not a valid file"
    fi
}


# Some of my aliass
#alias ls="ls --group-directories-first --color=always"
alias tlynx="torsocks lynx"
alias cp="cp -iv"
alias mv="mv -iv"
alias enw="emacsclient -t"
alias tboat="torsocks newsboat"
alias tcurl="torsocks curl"
alias tt="torsocks toot"
alias tmux="tmux -2"
# alias battery="acpi -i"
alias lock="xscreensaver-command -lock"
alias find-pac='find /etc -regextype posix-extended -regex ".+\.pac(new|save)" 2> /dev/null'
alias grep-pac='grep --extended-regexp "\.pac(new|save)" /var/log/pacman.log | tail'
alias tlmgr='/usr/share/texmf-dist/scripts/texlive/tlmgr.pl --usermode'
alias fedi="toot post -e nano"

## Youtube-dl

alias ytw="mpv --ytdl-raw-options=format=18"
alias yta-vorbis="youtube-dl --extract-audio --audio-format vorbis"
alias yta-best="youtube-dl --extract-audio --audio-format best"
alias mpg="mpv --player-operation-mode=pseudo-gui"

# Seting random  wallpaper
alias ran-wp="nitrogen --random --set-scaled ~/Pictures/Wallpaper"

# Distrotube's aliases
alias pacsyu="doas pacman -Syu"

export ALTERNATIVE_EDITOR="nano"
export EDITOR="emacsclient"
export VISUAL="emacsclient"
#export EDITOR="nano"

## Adding path
export PATH="home/starfish/.gem/ruby/3.0.0/bin:$PATH"
export PATH="/home/starfish/.emacs.d/bin:$PATH"
export PATH="/home/starfish/bin:$PATH"


## This is for changing the keybindings

# Swapping CapsLock and Cntrol [Experimental]

setxkbmap -option ctrl:nocaps
setxkbmap -option ctrl:swapcaps
#complete -c pass
# Launching tmux at every login shell in graphical enviroment
##if command -v tmux >/dev/null 2>&1 && [ "${DISPLAY}" ]; then
    # if not inside a tmux session, and if no session is started, start a new session
#    [ -z "${TMUX}" ] && (tmux attach || tmux)
#fi
