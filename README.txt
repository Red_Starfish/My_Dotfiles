

Copyright (C) 2020-2021 Abhiseck Paira

This repository is under X Consortium license. See the COPYING file in
this repository for terms of copying. Individual files may have
their own license headers. If they do that license overrides the X
Consortium license.

-----
This repository contains my "Dotfiles", i.e., Configuration file for
certain programs that I use. This repository is created using GNU Stow.

# Configuration Files of Openbox

* menu.xml
* rc.xml
* autostart

# Configuration file for tint2 panel

* tint2rc (Also inside the openbox directory)

# Configuration file GNU Emacs

* init.el
* my-funks.el (Some custom function definition)
* my-keybindings.el
* stfh-macros.el (Some keyboard macros)


