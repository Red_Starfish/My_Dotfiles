# Created by newuser for 5.8
# Lines configured by zsh-newuser-install
HISTFILE=~/.histfile
HISTSIZE=1000
SAVEHIST=1000
# prevent duplications
setopt hist_ignore_all_dups

export GUIX_PROFILE="/home/starfish/.guix-profile"
source "$GUIX_PROFILE/etc/profile"

export GUIX_PROFILE="$HOME/.config/guix/current"
source "$GUIX_PROFILE/etc/profile"

export GUIX_LOCPATH=$HOME/.guix-profile/lib/locale
export SSL_CERT_DIR="$HOME/.guix-profile/etc/ssl/certs"
export SSL_CERT_FILE="$HOME/.guix-profile/etc/ssl/certs/ca-certificates.crt"
export GIT_SSL_CAINFO="$SSL_CERT_FILE"
export CURL_CA_BUNDLE="$HOME/.guix-profile/etc/ssl/certs/ca-certificates.crt"

# Ignore entries that start with space
setopt hist_ignore_space

# Auto cd
setopt autocd


bindkey -e
# End of lines configured by zsh-newuser-install
# The following lines were added by compinstall
zstyle :compinstall filename '/home/starfish/.zshrc'

autoload -Uz compinit
compinit
# End of lines added by compinstall
# Default tab completion is quite plain and ugly
zstyle ':completion:*:descriptions' format '%U%B%d%b%u'
zstyle ':completion:*:warnings' format '%BSorry, no matches for: %d%b'

# It's good idea to autocorrect commands
setopt correctall

export TERM="xterm-256color"

# Enable fortune
fortune -a

# configuring the prompts
BLUE="\[$(tput setaf 14)\]"
PINK="\[$(tput setaf 5)\]"
RESET="\[$(tput sgr0)\]"
GREEN="\[$(tput setaf 2)\]"
GREY="\[$(tput setaf 8)\]"
RED="\[$(tput setaf 1)\]"

#PS1="${GREEN}[${RESET}${BLUE}\u${RESET}${PINK}@${RESET}${GREY}\h${RESET} ${RED}\W${RESET}${GREEN}]${RESET}{GNU} "

setprompt() {
  setopt prompt_subst

  if [[ -n "$SSH_CLIENT"  ||  -n "$SSH2_CLIENT" ]]; then
    p_host='%F{yellow}%M%f'
  else
    p_host='%F{yellow}%M%f'
  fi

  PS1=${(j::Q)${(Z:Cn:):-$'
    %F{cyan}[%f
    %(!.%F{red}%n%f.%F{green}%n%f)
    %F{cyan}@%f
    ${p_host}
    %F{cyan}][%f
    %F{blue}%~%f
    %F{cyan}]%f
    %(!.%F{red}%#%f.%F{pink}{GNU}%f)
    " "
  '}}

  PS2=$'%_>'
  RPROMPT=$'${vcs_info_msg_0_}'
}
setprompt


#PS1='[\u@\h \W]\$ ' # this is the main(primary) prompt


# Aliases
alias ls="ls --group-directories-first --color --file-type"
alias lt="ls --color -t"
alias ll="ls -l"
alias la="ls -a"
alias lla="ls -la"
alias grep="grep --color=always"
alias enw="emacsclient -t"
alias tlynx="torsocks lynx"
alias cp="cp -iv"
alias mv="mv -iv"
alias rm="rm -v"
alias mkdir="mkdir -v"
alias tboat="torsocks newsboat"
alias tcurl="torsocks curl"
alias tt="torsocks toot"
alias tmux="tmux -2"
# alias lock="xscreensaver-command -lock"
alias find-pac='find /etc -regextype posix-extended -regex ".+\.pac(new|save)" 2> /dev/null'
alias grep-pac='grep --extended-regexp "\.pac(new|save)" /var/log/pacman.log | tail'
alias tlmgr='/usr/share/texmf-dist/scripts/texlive/tlmgr.pl --usermode'
alias fedi="toot post -e nano"
alias twget="torsocks wget"
alias poff="doas poweroff"
alias reboo="doas reboot"
alias stow="stow --dotfiles --verbose=2"
alias ...="cd ../.."
alias tree="tree -a"
## Youtube-dl

alias ytw="mpv --ytdl-raw-options=format=18"
alias yta-vorbis="youtube-dl --extract-audio --audio-format vorbis"
alias yta-best="youtube-dl --extract-audio --audio-format best"
alias ffmpeg="ffmpeg -hide_banner"

alias ran-wp="nitrogen --random --set-scaled ~/Pictures/Wallpaper"
alias mpg="mpv --player-operation-mode=pseudo-gui"

alias pacsyu="doas pacman -Syu"

export ALTERNATIVE_EDITOR="nano"
export EDITOR="emacsclient"
export VISUAL="emacsclient -c"
export PATH="/home/starfish/.emacs.d/bin:$PATH"
export PATH="home/starfish/.gem/ruby/3.0.0/bin:$PATH"
export PATH="/home/starfish/bin:$PATH"
export PATH="/home/starfish/.mozbuild/git-cinnabar:$PATH"
export PATH="/usr/local/texlive/2020/bin/x86_64-linux:$PATH"

# Swapping CapsLock and Cntrol [Experimental]

#setxkbmap -option ctrl:nocaps
#setxkbmap -option ctrl:swapcaps


# Start a tmux session if one isn't running

if command -v tmux >/dev/null 2>&1 && [ "${DISPLAY}" ]; then
    # if not inside a tmux session, and if no session is started, start a new session
    [ -z "${TMUX}" ] && (tmux attach || tmux)
fi

ex ()
{
    if [ -f $1 ] ; then
	case $1 in
	    *.tar.bz2)   tar xjf $1   ;;
	    *.tar.gz)    tar xzf $1   ;;
	    *.bz2)       bunzip2 $1   ;;
	    *.rar)       unrar x $1   ;;
	    *.gz)        gunzip $1    ;;
	    *.tar)       tar xf $1    ;;
	    *.tbz2)      tar xjf $1   ;;
	    *.tgz)       tar xzf $1   ;;
	    *.zip)       unzip $1     ;;
	    *.Z)         uncompress $1;;
	    *.7z)        7z x $1      ;;
	    *.deb)       ar x $1      ;;
	    *.tar.xz)    tar xf $1    ;;
	    *.tar.zst)   unzstd $1    ;;
            *)           echo "'$1' cannot be extracted via ex()" ;;
	esac
    else
	echo "'$1' is not a valid file"
    fi
}

# Syntax highlight and autosuggesions
source /usr/share/zsh/plugins/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh
source /usr/share/zsh/plugins/zsh-autosuggestions/zsh-autosuggestions.plugin.zsh
