;;; my-funks.el --- Description -*- lexical-binding: t;  -*-
;;
;; Copyright (C) 2021 Abhiseck Paira <abhiseckpaira@disroot.org>
;;
;; This is free and unencumbered software released into the public
;; domain.

;; Anyone is free to copy, modify, publish, use, compile, sell, or
;; distribute this software, either in source code form or as a
;; compiled binary, for any purpose, commercial or non-commercial, and
;; by any means.
;;
;; In jurisdictions that recognize copyright laws, the author or
;; authors of this software dedicate any and all copyright interest in
;; the software to the public domain. We make this dedication for the
;; benefit of the public at large and to the detriment of our heirs
;; and successors. We intend this dedication to be an overt act of
;; relinquishment in perpetuity of all present and future rights to
;; this software under copyright law.
;;
;; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;; EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;; MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
;; NONINFRINGEMENT.  IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY
;; CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF
;; CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
;; WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
;;
;; Author: Abhiseck Paira
;; Maintainer:  Abhiseck Paira <abhiseckpaira@disroot.org>
;; Created: April 07, 2021
;; Modified: May 30, 2021
;; Keywords: my-funks, .emacs
;; Package-Requires: ((emacs "24.3"))
;;;
;;;  Description:
;; Contains some of the functions I wrote make my experience
;; pleasureable with Emacs.
;;
;;; Code:

;; A minor mode to hide the modeline
(define-minor-mode hidden-mode-line-mode
  "Hides the mode-line.
Toggle Hidden-Mode-Line mode on or off. If called interactively,
enable Hidden-Mode-Line mode if ARG is positive, and disable it
if ARG is zero or negative. If called from Lisp, also enable the
mode if ARG is omitted or nil, and toggle it if ARG is toggle;
disable the mode otherwise."
  :init-value nil
  (if hidden-mode-line-mode
      (setq hide-mode-line mode-line-format
            mode-line-format nil)
    (setq mode-line-format hide-mode-line
          hide-mode-line nil))
  (force-mode-line-update)
  (redraw-display))

(define-minor-mode show-trailing-whitespace-mode
  "Minor mode to Control the `show-trailing-whitespace' variable.
Toggle show-trailing-whitespace-mode on or off. If called
interactively, enable show-trailing-whitespace mode if ARG is
positive, and disable it if ARG is zero or negative. If called
from Lisp, also enable the mode if ARG is omitted or nil, and
toggle it if ARG is toggle; disable the mode otherwise."
  :init-value nil
  :lighter " whitespace"
  (if show-trailing-whitespace-mode
      (setq show-trailing-whitespace t)
    (setq show-trailing-whitespace nil)))

(define-minor-mode indicate-empty-lines-mode
  "Minor mode to Control the `indicate-empty-lines' variable.
Toggle indicate-white-lines-mode on or off. If called
interactively, enable indicate-white-lines mode if ARG is
positive, and disable it if ARG is zero or negative. If called
from Lisp, also enable the mode if ARG is omitted or nil, and
toggle it if ARG is toggle; disable the mode otherwise."
  :init-value nil
  :lighter " empty-lines"
  (if indicate-empty-lines-mode
      (setq indicate-empty-lines t)
    (setq indicate-empty-lines nil)))


(defun sign-or-encrypt-message ()
  "This function asks to sign or encrypt an outgoing email."
  (let ((answer
	 (read-from-minibuffer "Sign or encrypt?\nEmpty to do nothing.\n[s/e]: ")))
    (cond
     ((string-equal answer "s")
      (message "Signing message.")
      (mml-secure-message-sign-pgpmime))
     ((string-equal answer "e")
      (message "Encrypt and signing message.")
      (mml-secure-message-encrypt-pgpmime))
     (t
      (message "Not signing or encrypting message.")
      nil))))

(add-hook 'message-send-hook #'sign-or-encrypt-message)

(defun stfh-remove-minor-mode-modeline ()
  "Removes enabled minor-modes information from the modeline.
This function modifies the `minor-mode-alist' variable and puts a blank
string as the VALUE of the KEYS."
  (interactive)
  (let ((list-minor-modes (mapcar 'car minor-mode-alist)))
    (dolist (minor-mode list-minor-modes)
      (setcar (alist-get minor-mode minor-mode-alist) ""))))

;; Run this command after change in major modes
(add-hook 'after-change-major-mode-hook
	  #'stfh-remove-minor-mode-modeline)


(define-minor-mode stfh-wrap-my-line-mode
  "Minor-mode to enable `visual-line-mode'.
When it turns on the visual-line-mode it simultaneously disables
`auto-fill-mode' in that buffer. When this mode is disabled it
disables the visual-line-mode and enables the auto-fill-mode
again."
  :init-value nil
  :lighter " stfh-wrap"
  (if stfh-wrap-my-line-mode
      (progn (visual-line-mode 1)
	     (auto-fill-mode 0))
    (visual-line-mode 0)
    (auto-fill-mode 1)))

;; Doom modeline conditonal buffer encoding

;; (defun doom-modeline-conditional-buffer-encoding ()
;;   "We expect the encoding to be LF UTF-8, so only show the modeline when this is not the case."
;;   (setq-local doom-modeline-buffer-encoding
;;               (unless (or (eq buffer-file-coding-system 'utf-8-unix)
;;                           (eq buffer-file-coding-system 'utf-8)))))

;; (add-hook 'after-change-major-mode-hook #'doom-modeline-conditional-buffer-encoding)


(provide 'my-funks)
