;; -*- lexical-binding: t;

;; My defined keys
(global-set-key (kbd "C-x p r") 'rename-buffer)
(global-set-key (kbd "C-x p u") 'rename-uniquely)
(global-set-key (kbd "C-x p t") 'load-theme)
(global-set-key (kbd "C-x p s") 'shell)
(global-set-key (kbd "C-x p e") 'eshell)
(global-set-key (kbd "C-x p m") 'man)
(global-set-key (kbd "C-x p b") 'battery)
(global-set-key (kbd "C-x p p") 'proced)
(global-set-key (kbd "C-x p o") 'recentf-open-files)
(global-set-key (kbd "C-x p w") #'stfh-wrap-my-line)
(global-set-key (kbd "M-z") 'zap-up-to-char)
(global-set-key (kbd "C-x p c z") 'zap-to-char)
(global-set-key (kbd "C-x 4 v") 'view-file-other-window)
(global-set-key (kbd "C-x r p s") 'bookmark-save)
(global-set-key (kbd "C-x r p d") 'bookmark-delete)
(global-set-key (kbd "C-x r p r") 'bookmark-rename)
(global-set-key (kbd "C-x C-b") 'ibuffer-other-window)
(global-set-key (kbd "C-x a u") 'unexpand-abbrev)
(global-set-key (kbd "C-x C-k P") #'macro-narrow-to-paragraph)

;; For info-mode map
(add-hook 'Info-mode-hook
	  (lambda ()
	    (emojify-mode 0)
	    (define-key Info-mode-map (kbd "C-h v")
	      'counsel-describe-variable)))

;; For prog-mode-map
(add-hook 'prog-mode-hook
	  (lambda ()
	    (define-key prog-mode-map (kbd "C-c C-;")
	      'uncomment-region)
	    (define-key prog-mode-map (kbd "C-c ;")
	      'comment-region)))
	      
;; dired mode map
(add-hook 'dired-mode-hook
	  (lambda ()
	    (define-key dired-mode-map (kbd "h")
	      'dired-up-directory)
	    (define-key dired-mode-map (kbd "/ k")
	      'dired-kill-subdir)))

;; View mode bindings
(add-hook 'view-mode-hook
	  (lambda ()
	    ;; bind s to swiper-isearch command
	    (define-key view-mode-map (kbd "s")
	      'swiper-isearch)))

;; mu4e-headers mode hook
(add-hook 'mu4e-headers-mode-hook
	  (lambda ()
	    (define-key mu4e-headers-mode-map
	      (kbd "C-c p i")
	      'mu4e-update-index)))
;; Ivy based interface to standard commands


(global-set-key (kbd "C-s") 'swiper-isearch)
(global-set-key (kbd "M-x") 'counsel-M-x)
(global-set-key (kbd "C-x C-f") 'counsel-find-file)
(global-set-key (kbd "M-y") 'counsel-yank-pop)
(global-set-key (kbd "<f1> f") 'counsel-describe-function)
(global-set-key (kbd "<f1> v") 'counsel-describe-variable)
(global-set-key (kbd "<f1> l") 'counsel-find-library)
(global-set-key (kbd "<f2> i") 'counsel-info-lookup-symbol)
(global-set-key (kbd "<f2> u") 'counsel-unicode-char)
(global-set-key (kbd "<f2> j") 'counsel-set-variable)
(global-set-key (kbd "C-x b") 'ivy-switch-buffer)
(global-set-key (kbd "C-c v") 'ivy-push-view)
(global-set-key (kbd "C-c V") 'ivy-pop-view)

;; Ivy based interface and system tools

(global-set-key (kbd "C-c C") 'counsel-compile)
(global-set-key (kbd "C-c g") 'counsel-git)
(global-set-key (kbd "C-c j") 'counsel-git-grep)
(global-set-key (kbd "C-c L") 'counsel-git-log)
(global-set-key (kbd "C-c k") 'counsel-rg)
(global-set-key (kbd "C-c m") 'counsel-linux-app)
(global-set-key (kbd "C-c n") 'counsel-fzf)
(global-set-key (kbd "C-x l") 'counsel-locate)
(global-set-key (kbd "C-c J") 'counsel-file-jump)
(global-set-key (kbd "C-S-o") 'counsel-rhythmbox)
(global-set-key (kbd "C-c w") 'counsel-wmctrl)

;; Ivy resume and other commands

(global-set-key (kbd "C-c C-r") 'ivy-resume)
(global-set-key (kbd "C-c B") 'counsel-bookmark)
(global-set-key (kbd "C-c d") 'counsel-descbinds)
(global-set-key (kbd "C-c o") 'counsel-outline)
(global-set-key (kbd "C-c T") 'counsel-load-theme)
(global-set-key (kbd "C-c F") 'counsel-org-file)


;; Helpful keybindings
(global-set-key (kbd "C-h f") #'helpful-callable)
(global-set-key (kbd "C-h v") #'helpful-variable)
(global-set-key (kbd "C-h k") #'helpful-key)
(global-set-key (kbd "C-c C-d") #'helpful-at-point)
(global-set-key (kbd "C-h F") #'helpful-function)
(global-set-key (kbd "C-h C") #'helpful-command)

;; In info-mode disable the helpful-variable binding
;; Elfeed mode
(global-set-key (kbd "C-x w") 'elfeed)

;;(global-set-key (kbd "C-c l") 'org-store-link)
(global-set-key (kbd "C-c a") 'org-agenda)
;;(global-set-key (kbd "C-c c") 'org-capture)
;;(global-set-key "\C-cb" 'org-iswitchb)
;;(define-key global-map (kbd "C-c t") telega-prefix-map)


(provide 'my-keybindings)
