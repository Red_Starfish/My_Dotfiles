;; -*- lexical-binding: t; -*-

(fset 'macro-no-fill
      (kmacro-lambda-form [?\C-a backspace ?  ?\C-n ?\C-a backspace ?  ?\C-n ?\C-n] 0 "%d"))

(fset 'macro-narrow-to-paragraph
   (kmacro-lambda-form [?\M-\{ ?\C-  ?\M-\} ?\C-x ?n ?n ?\C-x ?\C-x ?\C-  ?\C- ] 0 "%d"))

(provide 'stfh-macros)
