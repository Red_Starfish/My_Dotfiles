;;; init.el --- Description -*- lexical-binding: t;  -*-
;;
;; Copyright (C) 2021 Abhiseck Paira <abhiseckpaira@disroot.org>
;;
;; This is free and unencumbered software released into the public
;; domain.
;;
;; Anyone is free to copy, modify, publish, use, compile, sell, or
;; distribute this software, either in source code form or as a
;; compiled binary, for any purpose, commercial or non-commercial, and
;; by any means.
;;
;; In jurisdictions that recognize copyright laws, the author or
;; authors of this software dedicate any and all copyright interest in
;; the software to the public domain. We make this dedication for the
;; benefit of the public at large and to the detriment of our heirs
;; and successors. We intend this dedication to be an overt act of
;; relinquishment in perpetuity of all present and future rights to
;; this software under copyright law.
;;
;; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;; EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;; MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
;; NONINFRINGEMENT.  IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY
;; CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF
;; CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
;; WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
;;
;; Author: Abhiseck Paira
;; Maintainer: Abhiseck Paira <abhiseckpaira@disroot.org>
;; Created: April 07, 2021
;; Modified: May 30, 2021
;; Keywords: init.el, .emacs
;; Package-Requires: ((emacs "24.3"))
;;
;;; Commentary:
;;; My init file
;;
;;; Code:

(require 'package)
(add-to-list
 'package-archives
 ;; '("melpa" . "http://stable.melpa.org/packages/") ; many packages won't show if using stable
 '("melpa" . "https://melpa.org/packages/")
 t)

;; Load the custom file
(setq custom-file "~/.emacs.d/emacs-custom.el")
(load custom-file)

;; Add this directory load-path variable
(add-to-list 'load-path "/home/starfish/.emacs.d/my-files/")
;; They say it's the most recent changes
;; Disable the splash screen (to enable it agin, replace the t with 0)
(setq inhibit-splash-screen t) ; replacing t with nil makes the startup
;; screen appear again

;; Don't use messages you don't read
(setq initial-scratch-message
      ";; Welcome to GNU Emacs! One component of GNU/Linux operating system.")
(setq inhibit-startup-message t)

;; Don't let Emacs hurt your eyes
(setq visible-bell t)

;; Remove the Menubar
(menu-bar-mode 0)

;; Use doom-modeline-mode
;;(doom-modeline-mode 1)
;;(setq doom-modeline-height 10)

;; settings borrowed from custom
(set-face-attribute 'default nil :family "Source Code Pro" :weight 'semi-bold :height 114 :width 'normal)
(setq ansi-color-faces-vector
      [default default default italic underline success warning error])
(setq auth-source-save-behavior nil)
(column-number-mode 1)
(display-time-mode 1)
;;(ivy-prescient-mode 0)
;;(setq custom-enabled-themes '(wombat))
;;(display-battery-mode 1)
(setq custom-safe-themes t)

;; Theme settings
(require 'modus-themes)
(setq modus-themes-mode-line 'accented-3d
      modus-themes-bold-constructs t
      modus-themes-slanted-constructs t
      modus-themes-links 'faint-neutral-underline
      modus-themes-prompts 'intense-accented
      modus-themes-completions 'opinionated
      modus-themes-paren-match 'intense-bold
      modus-themes-org-blocks 'rainbow
      modus-themes-scale-headings t
      modus-themes-scale-1 1.12
      modus-themes-scale-2 1.14
      modus-themes-scale-3 1.16
      modus-themes-scale-4 1.18
      modus-themes-scale-5 1.22
      modus-themes-variable-pitch-ui t
      modus-themes-variable-pitch-headings t)
;;
(load-theme 'modus-operandi)
;;(load-theme 'adwaita)
(scroll-bar-mode 0)
(tool-bar-mode 0) ;; Remove toolbar
(good-scroll-mode 1)
;; (global-hl-line-mode 1) ; Highlight the cursor
(setq org-agenda-files '("~/org-mode/notes/notes.org" "~/org-mode/uits-3.org"))

;; Load the my custom keybindings
(require 'my-keybindings)

;; Ranbow delimiters other paren settings
(dolist (func '(rainbow-delimiters-mode
		show-paren-mode
		electric-pair-mode
		linum-mode
		flyspell-prog-mode
		show-trailing-whitespace-mode
		indicate-empty-lines-mode))
  (add-hook 'prog-mode-hook func))

;; Enable company mode globally
(add-hook 'after-init-hook #'global-company-mode)

(setq auth-sources '("~/.authinfo.gpg" "~/.authinfo" "~/.netrc"))
;; Enable transient mark mode
(transient-mark-mode 1)

;; Make text-mode as the default major mode
(setq default-major-mode 'org-mode)

(add-hook 'text-mode-hook 'turn-on-auto-fill)
(add-hook 'text-mode-hook 'flyspell-mode)
(add-hook 'text-mode-hook (lambda () (set-fill-column 72)))
(require 'company-box)
(add-hook 'company-mode-hook #'company-box-mode)

;; Add symon

(setq proced-auto-update-interval 2)
(add-hook 'proced-mode-hook (lambda ()
			      (proced-toggle-auto-update 1)))

;; Symon mode settings
;; (require 'symon)
;; (setq symon-delay 4)
;; (symon-mode 1)
(add-hook 'shell-mode-hook #'visual-line-mode)

;; Start Word Abbreviation mode on startup
(setq-default abbrev-mode t)
(read-abbrev-file "~/.emacs.d/abbrev_defs")
(setq save-abbrevs t)

;; To set the frame size. I currently have the right frame size so
;; I'll disable them
;(setq initial-frame-alist '((top . 10) (left . 30)
;			    (width . 90) (height . 50)))
;(setq default-frame-alist '((width . 80) (height . 45)))

;;;;Org mode configuration
;; Enable Org mode
(require 'org)
;; Make Org mode work with files ending in .org
;; (add-to-list 'auto-mode-alist '("\\.org$" . org-mode))
;; The above is the default in recent emacsen
(setq org-todo-keywords
      '((sequence "TODO" "IN-PROGRESS" "WAITING" "DONE")))
;; This is for org-mode agenda view activation
;;; (add-to-list 'auto-mode-alist '("\\.org\\'" . org-mode))
;; This is for key bindings to invoke agenda mode (see line-2)

(setq counsel-describe-function-function #'helpful-callable)
(setq counsel-describe-variable-function #'helpful-variable)

;; Keep track of time when a TODO was marked as DONE
(setq org-log-done 'time)
;; Also optionally record a note
(setq org-log-done 'note)

;; Ebook settings
(add-to-list 'auto-mode-alist '("\\.epub\\'" . nov-mode))
(setq nov-text-width 70
      nov-variable-pitch nil)


;; If you want to hide the mode-line in every buffer by default
;; (add-hook 'after-change-major-mode-hook 'hidden-mode-line-mode)

;; Tell emacs to hide password as you type them in the shell enviroment
(add-hook 'comint-output-filter-functions
	  'comint-watch-for-password-prompt)

;; Use the bash shell
(setq shell-file-name "/bin/bash"
      explicit-shell-file-name "/bin/bash"
      shell-command-switch "-ic"
      shell-command-default-error-buffer "Shell Command Errors"
      ;; Make the M-! show current directory in command prompt
      shell-command-prompt-show-cwd t)

;; Start calender on Monday
(setq calendar-week-start-day 1
      ;; Please use European date formats
      european-calendar-style t)
      ;; Enable document parsing in latex
;; Activate pdf tools
(pdf-tools-install)

;; Update PDF buffers after successful LaTeX runs
(add-hook 'TeX-after-compilation-finished-functions
          #'TeX-revert-document-buffer)

(setq TeX-auto-save t
      Tex-parse-self t
      ;; Use pdf-tools to open PDF files
      TeX-view-program-selection '((output-pdf "PDF Tools"))
      TeX-source-correlate-start-server t
      pdf-view-resize-factor 1.1)


;; Deactivate linum-mode when using pdf-tools
(add-hook 'pdf-view-mode-hook (lambda() (linum-mode -1)))

;; Mastodon instance
(setq mastodon-instance-url "https://social.linux.pizza"
      mastodon-auth-source-file "~/.authinfo")

;; Globally activate emojis
(add-hook 'after-init-hook #'global-emojify-mode)

;;(add-hook 'after-init-hook #'global-emojify-line-mode)

;; Use org-superstar mode when using org mode
(add-hook 'org-mode-hook 'org-superstar-mode)

;; Enable ivy completions everywhere
(ivy-mode 1)
;; Enable which-key mode
(which-key-mode 1)

;; Some ivy customization
(setq ivy-use-virtual-buffers t)
(setq ivy-count-format "(%d/%d) ")

;; Make gc pauses faster by decreasing the threshold.
(setq gc-cons-threshold (* 2 1000 1000)
      ;; Wait for 2 seconds between steps in trace mode
      edebug-sit-for-seconds 2

      ;; Dired settings
      dired-listing-switches "-ahtl --group-directories-first"
      dired-create-destination-dirs 'ask

      ;; Recentf settings
      recentf-max-menu-items 25
      recentf-max-saved-items 25

      ;; Mark ring settings
      set-mark-command-repeat-pop t

      ;; Kill ring settings
      kill-read-only-ok t
      kill-do-not-save-duplicates t

      ;; The number of lines of overlap left by these scroll commands
      next-screen-context-lines 3
      scroll-preserve-screen-position nil

      ;; Save bookmark immediately
      bookmark-save-flag 1

      ;; Text scaling factor
      text-scale-mode-step 1.02

      ;; Battery mode
      battery-mode-line-format " %b%p%%"

      ;; Set the yank pop separator.
      counsel-yank-pop-separator "\n--------\n")

;;
;; Elfeed feeds
;;
(setq-default elfeed-search-filter "@1-week-ago +unread")
(require 'elfeed-feeds-list)

;;
;; MU4E settings
;;
(require 'mu4e)
(require 'org-mu4e)
(require 'mu4e-contrib)
(require 'smtpmail)

(setq message-kill-buffer-on-exit t
      ;; The variable ‘message-send-mail-function’ controls how the message
      ;; is delivered. (emacs)Top > Sending Mail > Mail Commands > Mail
      ;; Sending
      message-send-mail-function 'smtpmail-send-it
      mu4e-attachment-dir "~/Downloads"
      mu4e-change-filenames-when-moving t
      mu4e-completing-read-function 'completing-read
      mu4e-compose-complete-addresses t
      mu4e-compose-context-policy nil
      mu4e-compose-dont-reply-to-self t
      mu4e-compose-keep-self-cc nil
      mu4e-context-policy 'pick-first
      mu4e-get-mail-command "mbsync -a"
      mu4e-update-interval (* 10 60)
      mu4e-headers-date-format "%d-%m-%Y %H:%M"
      mu4e-headers-fields '((:human-date . 20)
                            (:flags . 6)
                            (:mailing-list . 10)
                            (:from . 22)
                            (:subject))
      mu4e-headers-include-related t
      mu4e-view-show-addresses t
      mu4e-view-show-images t
      ;; Only ask if a context hasn't been previously picked
      mu4e-compose-context-policy 'ask-if-none
      ;; This is set to 't' to avoid mail syncing issues when using mbsync
      mu4e-change-filenames-when-moving t
      ;; Refresh mail using isync every 10 minutes
      mu4e-update-interval nil
      mu4e-get-mail-command "mbsync -a"
      mu4e-maildir "~/.local/share/mail"
      mu4e-sent-messages-behavior 'sent
      message-cite-reply-position 'below
      ;; Configure the function to use for sending mail
      message-send-mail-function 'smtpmail-send-it
      mm-sign-option 'guided
      smtpmail-debug-info t)

(when (fboundp 'imagemagick-register-types)
  (imagemagick-register-types))

;; Make sure plain text mails flow correctly for recipients

;; Load contexts from ~/.emacs.d/my-files/mu4e-my-contexts.el
(require 'mu4e-my-contexts)

;; Load the custom functions
;; ~/.emacs.d/my-files/my-funks.el
(require 'my-funks)
(add-hook 'pdf-view-mode-hook #'hidden-mode-line-mode)
;; Load my macro files
(require 'stfh-macros)

;; List of disabled commands that have been enabled.
(put 'downcase-region 'disabled nil)
(put 'upcase-region 'disabled nil)
(put 'narrow-to-page 'disabled nil)
(put 'dired-find-alternate-file 'disabled nil)
(put 'narrow-to-region 'disabled nil)

